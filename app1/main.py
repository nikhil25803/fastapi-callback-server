import random
from fastapi import FastAPI, Request
import json
from fastapi.responses import JSONResponse
import time, asyncio

app = FastAPI()


async def random_status_code():
    return random.choice([200, 201, 202, 400, 401, 500])

async def timeout():
    await asyncio.sleep(5)

@app.get("/")
async def index():
    return {"message": "Application 1 up and running"}


@app.post("/app1")
async def mock_endpoint(request: Request):
    headers = request.headers
    body = await request.json()

    status_code = await random_status_code()

    # time.sleep(20)
    await timeout()
    return JSONResponse(
        content={
            "message": "Response from application 1",
            "headers": dict(headers),
            "body": json.dumps(body),
        },
        status_code=200,
    )
