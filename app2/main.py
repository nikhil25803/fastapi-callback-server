import random
from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
import json

app = FastAPI()


async def random_status_code():
    return random.choice([200, 201, 202, 400, 401, 500])


@app.get("/")
async def index():
    return {"message": "Application 2 up and running"}


@app.post("/app2")
async def mock_endpoint(request: Request):
    headers = request.headers
    body = await request.json()

    status_code = await random_status_code()

    return JSONResponse(
        content={
            "message": "Response from application 2",
            "headers": dict(headers),
            "body": json.dumps(body),
        },
        status_code=200,
    )


# if __name__ == "__main__":
#     uvicorn.run(app, host="0.0.0.0", port=8002)
