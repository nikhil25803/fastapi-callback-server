from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
import httpx, json, asyncio

app = FastAPI()

DEFINED_ENDPOINTS = ["http://127.0.0.1:8001/app1", "http://127.0.0.1:8002/app2"]


async def async_request_processing(url: str, request: Request):
    async with httpx.AsyncClient() as client:
        response = await client.request(
            request.method, url, headers=request.headers, data=await request.body()
        )

        response_data = {
            "status_code": response.status_code,
            "headers": dict(response.headers),
            "content": response.text,
        }
        return response_data


@app.get("/")
async def index():
    return {"message": "Server is up and running"}


async def fetch(request: Request):
    endpoints = DEFINED_ENDPOINTS

    async with httpx.AsyncClient() as client:
        reqs = [async_request_processing(url, request) for url in endpoints]

        for task in asyncio.as_completed(reqs):
            response = await task
            if response.get("status_code") in [200, 201, 202]:
                return [response]

        result = await asyncio.gather(*reqs)

        return result


@app.post("/test")
async def forward_to_endpoints(request: Request):
    responses = await fetch(request)

    # Check responses for success codes (200, 201, 202)
    success_responses = [
        resp for resp in responses if resp.get("status_code") in [200, 201, 202]
    ]

    if success_responses:
        response = success_responses[0]
    else:
        response = responses[0]

    response["content"] = json.loads(response["content"])
    response["content"]["body"] = json.loads(response["content"]["body"])

    return JSONResponse(content={"data": response}, status_code=200)
